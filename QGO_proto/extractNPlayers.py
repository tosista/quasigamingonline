from extractPlayer import extractPlayer

def extractNPlayers(n):
    players = []
    for i in range(n):
        tmp_player = extractPlayer()
        players.append(tmp_player)

    return players
