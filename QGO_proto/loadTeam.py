from classes.Team import Team
import daoTeam as dt


def loadTeam():

    # fetch teams name and print list
    teams = dt.readTeamNames()
    for t in teams:
        print(t)

    team_to_load: str = input('Team name you want to load: ')
    if team_to_load in teams:
        print("Team found, loading!")
        id_team = dt.findTeamId(team_to_load)
        team:Team = dt.readTeam(id_team)
    else:
       raise Exception

    return team