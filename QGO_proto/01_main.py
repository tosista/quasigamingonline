from makeTeam import makeTeam
from loadTeam import loadTeam
import daoTeam as dt
import daoLineup as dl

menu = 'QuasiGaming\n\nn - create new team\no - open a team\nq - quit'

players = []
team = ''

def print_all_lineups(team):
    for role, list in team.lineup.items():
        print(f'\n{role} lineup:')
        for id in list:
            print(team.get_player(id))


print('Welcome to QuasiGaming!\n\ntype "h" for help')

while True:
    cmd = input('> ')
    if cmd == 'h':
        print(menu)
    if cmd == 'n':
        team = makeTeam()
    elif cmd == 'o':
        try:
            team = loadTeam()
            print('loaded Team:')
            for p in team.players:
                print(p)
        except:
            print('Sorry, team not found')
    elif cmd == 'l':
        print_all_lineups(team)
        role = input("\nRole you want to edit: ")
        lineup_list = team.get_lineup(role.upper())
        print('\nThis is the current lineup:')
        for p in lineup_list:
            tmp_player = team.get_player(p)
            print(tmp_player)
        action = input('\nType A to add a player, or R to remove: ')
        if action.lower() == 'a':
            print('\nThese are the available players for this position:')
            for p in (team.get_role_players(role)):
                print(p)
            id_player = int(input("\nPlayer ID to add: "))
            try:
                team.add_lineup(role, id_player)
                dl.add_lineup(team.id, id_player, role)
                print(f"\nLineup updated, the current {role} lineup is: ")
                for p in (team.get_lineup(role)):
                    print(team.get_player(p))
            except:
                print('Error\n')
        elif action.lower() == 'r':
            id_player = int(input("\nPlayer ID to remove: "))
            try:
                team.rem_lineup(role, id_player)
                dl.rem_lineup(team.id, id_player)
                print(f"\nLineup updated, the current {role} lineup is: ")
                for p in (team.get_lineup(role)):
                    print(team.get_player(p))
            except:
                print('Error\n')
        else:
            print('command not found')
    elif cmd == 'p':
        print_all_lineups(team)
    elif cmd == 'e':
        # Extract
        pass
    elif cmd == 'q':
        confirm = input('ATTENTION: unused players will be removed! Do you want to continue? ')
        if confirm == 'y':
            unused_players = team.get_unused_players()
            for p in unused_players:
                dt.rem_team_player(team.id, p.id)
            break
        else:
            pass
    else:
        print('command not found')

print('Thank you, come again!')