from pymongo import MongoClient
from database.daoPlayers import readPlayers

players = readPlayers()

CONNECTION_STRING = "mongodb://127.0.0.1:27017/"

client = MongoClient(CONNECTION_STRING)

dbname = client['quasigaming']

collection = dbname['players']


for p in players:

    tmp_player = {
        "id": p.id,
        "name": p.name,
        "team": p.team,
        "pos": p.pos,
        "value": p.value,
        "extr": p.extr,
        "espn_id": p.espn_id,
        "img": f"https://a.espncdn.com/combiner/i?img=/i/headshots/nba/players/full/{p.espn_id}.png"
    }
    collection.insert_one(tmp_player)
    print(p, tmp_player)
