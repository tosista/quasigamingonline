import csv
from classes.Player import Player

def readCSV(filename):
    with open(filename, 'r', newline='') as f:
        # use the reader to connect to csv data
        rd = csv.reader(f, delimiter=',')
        # extract into a list the data from the reader
        matrix = list(rd)

    # fix last element of matrix being empty
    matrix = matrix[:-1]

    # creating an empy players list
    players_list = []

    # cycling throgh matrix to extract player info into dictionary
    id = 1
    for player in matrix:
        # unpack player list
        trash1, trash2, rank, name, team, pos, value, min_ex, max_ex = player

        # create new player
        temp_player = Player(id, rank, name, team, pos, value, min_ex, max_ex)

        id += 1

        players_list.append(temp_player)

    return players_list

if __name__ == '__main__':
    print('not an executable')