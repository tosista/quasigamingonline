import requests

folder = './logos'
saved = []

with open('teams.txt', 'r') as f:
    teams = f.readline()

teams = teams.split(',')[:-1]

print(teams)

for t in teams:
    uri = f"https://a.espncdn.com/i/teamlogos/nba/500/{t.lower()}.png"
    filename = f'{t}.png'
    response = requests.get(uri, allow_redirects=True)
    if response.ok:
        with open(filename, 'wb') as f:
            f.write(response.content)
            print(f'{t}logo saved')
print('\nAll done!')