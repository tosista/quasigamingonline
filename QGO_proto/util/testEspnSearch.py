import requests

# uri for searching
uri = "https://www.espn.com/search/_/q/"
headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"}
string = "AUSTIN RIVERS"
search_string = uri + string.replace(' ', '%20').lower()
print(search_string)
# Execute request
response = requests.get(search_string, headers=headers)
print(response.text)