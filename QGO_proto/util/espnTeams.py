import requests
import json
from database.getMongoClient import get_database

class NbaTeam:
    def __init__(self, id, abbreviation, displayName, color, altcolor, logo):
        self.id = id
        self.abbreviation = abbreviation
        self.displayName = displayName
        self.color = color
        self.altcolor = altcolor
        self.logo = logo

    def serialize(self):
        return {
            "id": self.id,
            "abbreviation": self.abbreviation,
            "displayName": self.displayName,
            "color": self.color,
            "altColor": self.altcolor,
            "logo": self.logo
        }

    def toJSON(self):
        return json.dumps(
            self,
            default=lambda o: o.__dict__,
            sort_keys=False,
            indent=4)

    def __str__(self):
        return f"id:{self.id}, abbreviation:{self.abbreviation}, displayName:{self.displayName}, color:{self.color}, altcolor:{self.altcolor}, logo:{self.logo}"


uri = "https://site.api.espn.com/apis/site/v2/sports/basketball/nba/teams"

response = requests.get(uri)
data = response.json()

teams = data["sports"][0]["leagues"][0]["teams"]
# print(teams)
print(type(teams))

teams_list = []
for t in teams:
    tmp_id = int(t["team"]["id"])
    tmp_abbr = str(t["team"]["abbreviation"])
    tmp_name = t["team"]["displayName"]
    tmp_color = t["team"]["color"]
    tmp_altcolor = t["team"]["alternateColor"]
    tmp_logo = t["team"]["logos"][0]["href"]
    print(f"{t["team"]["id"]}: {t["team"]["displayName"]}")
    temp_team = NbaTeam(tmp_id, tmp_abbr, tmp_name, tmp_color, tmp_altcolor, tmp_logo)
    teams_list.append(temp_team)

for t in teams_list:
    print(t.toJSON())


# try to save a json of the NbaTeam list
db = get_database()
coll = db['nbateams']
# cycle through teams list
for t in teams_list:
    coll.insert_one(t.serialize())