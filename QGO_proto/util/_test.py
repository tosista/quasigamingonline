from classes.Team import Team
from readingCSV import readCSV

players = readCSV('../test2.csv')

team = Team(players)

print(team)

print('\nSetting Centers')
team.add_C()

print('The players in C are:')
for p in team.C:
    print(p)