import sqlite3
from readingCSV import readCSV

players = readCSV('../../Matrice_2K24.csv')

conn = sqlite3.connect('../quasigaming.db')

cur = conn.cursor()

i = 1

for p in players:

    if len(p.pos) == 1:
        tmp_pos = p.pos[0]
    else:
        tmp_pos = f'{p.pos[0]}/{p.pos[1]}'

    sql = f"INSERT INTO players (id_player, name, team, pos, value, min_ex, max_ex)"\
        f' VALUES ({i}, "{p.name}", "{p.team}", "{tmp_pos}", {p.value}, {p.extr[0]}, {p.extr[1]});'

    cur.execute(sql)
    conn.commit()

    i += 1

cur.execute('SELECT * FROM magazzino;')

for row in cur.fetchall():
    id_player, name, team, pos, value, min_ex, max_ex = row
    print(f'{id_player}: {name}, {pos} from {team}, {value}')

conn.close()
