import requests
import json

teams = []

# list of found id-name pairings
found = {}

espn_teams = []

# fetch team names from .txt file
with open('teams.txt', 'r') as f:
    teams = f.readline()

teams = teams.split(',')[:-1]
teams.sort()

print(teams)

# GET PLAYERS IDs
for t in teams:
    uri = f"https://site.api.espn.com/apis/site/v2/sports/basketball/nba/teams/{t}/roster"
    response = requests.get(uri).json()
    list = response['athletes']
    for p in list:
        key = p['id']
        pl_name = p['displayName']
        if '.' in pl_name:
            pl_name = pl_name.replace('.','')
        found[key] = pl_name
    print(f'\n{t} Players:')
    for key, value in found.items():
        print(f'{key}: {value.upper()}')

    with open('espnIds.json', 'w') as f:
        json.dump(found, f)

'''
# GET TEAMS

for t in teams:
    uri = f"https://site.api.espn.com/apis/site/v2/sports/basketball/nba/teams/{t}/"
    response = requests.get(uri).json()
    object = response['team']
    info = {
        'id': object['id'],
        'code': object['abbreviation'],
        'name': object['displayName'],
        'image': f"https://a.espncdn.com/combiner/i?img=/i/teamlogos/nba/500/{object['abbreviation'].lower()}.png"
    }
    print(info)
    espn_teams.append(info)

with open('espn_teams.json', 'w') as f:
    json.dump(espn_teams, f)
'''