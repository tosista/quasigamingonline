import sqlite3
import json

# open connection to db
conn = sqlite3.connect('../quasigaming.db')
# create cursor to db
cur = conn.cursor()
# open json file
with open('espn_teams.json', 'r') as f:
    # read json content into dictionary
    data = json.load(f)
    # for each team in data
    for t in data:
        query = f"INSERT INTO nba_teams (id, code, name, image) VALUES ({t['id']}, '{t['code']}', '{t['name']}', '{t['image']}');"
        cur.execute(query)
        conn.commit()
    '''
    try:
        # for key, value in dictionary.items
        for key, value in data.items():
            # create query (insert into players (espn_id) values ({key}) where name = '{value}';)
            query = f"UPDATE players SET espn_id = {key} WHERE name = '{value.upper()}';"
            # execute query
            cur.execute(query)
            # conn.commit()
    except:
        print(f'Problems with {value}, id: {key}')
    '''
# close connection
conn.close()