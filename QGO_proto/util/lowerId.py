import json

keys = []

with open('espnIds.json', 'r') as f:
    data = json.load(f)
    for key, value in data.items():
        keys.append(int(key))

keys.sort()
print(keys)