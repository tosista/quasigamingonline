import requests
import json


folder = './headshots'
saved = 0

# import json file
with open('espnIds.json', 'r') as f:
    data = json.load(f)

for id, name in data.items():
    uri = f"https://a.espncdn.com/combiner/i?img=/i/headshots/nba/players/full/{id}.png&w=350&h=254"

    filename = f'{id}.png'
    response = requests.get(uri, allow_redirects=True)
    if response.ok:
        with open(folder+'/'+filename, 'wb') as f:
            f.write(response.content)
            print(f'{name} image saved')
            saved += 1
print('\nAll done!')