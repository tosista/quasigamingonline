import requests
import json

from classes.Player import Player
from database.getMongoClient import get_database

# Read teams from txt
espn_teams = []

# fetch team names from .txt file
with open('teams.txt', 'r') as f:
    teams = f.readline()

teams = teams.split(',')
teams.sort()

# Read athletes from txt
athletes = {}
with open('espnIdsMissing_2.txt', 'r') as f:
    for row in f.readlines():
        row = row.split(',')
        athletes[row[0]] = row[1][:-1]
print(athletes)

# add the free agents cathegory at the end
teams.append("FA")
print(teams)

# GET PLAYERS IDs
for t in teams:
    '''
    
    # list of found id: name pairings
    found = {}
    # input(f"\nCurrent team: {t} \n press return to go on")
    # GET request to ESPN API
    uri = f"https://site.api.espn.com/apis/site/v2/sports/basketball/nba/teams/{t}/roster"
    response = requests.get(uri).json()

    # Fetch list of athletes
    list = response['athletes']

    # Populate the found list with {id: name} dictionaries
    for p in list:
        key = p['id']
        pl_name = p['displayName']

        # remove the . from the player's name
        if '.' in pl_name:
            pl_name = pl_name.replace('.','')
        found[key] = pl_name

    # print the team players, their name and their id
    print(f'\n{t} Players:')
    for key, value in found.items():
        print(f'{key}: {value.upper()}')
    
    '''

    # open connection to db
    db = get_database()
    # create the collection for players
    coll = db['players']

    # Create list for null_id_players
    null_id_players = []

    # Find players from current team and with espn_id = None
    for p in coll.find({"espn_id": None}):
        null_id_players.append(p['name'])
    null_id_players.sort()

    # Check how many player still don't have espn_id
    print(f"\n\n--------------------------------------------------------------\n\nWe still miss {len(null_id_players)} players \n\n")
    for p in null_id_players:
        print(p)

    for key, value in athletes.items():
        if value.upper() in null_id_players:
            print(f"\n editing the id for {value.upper()}\n")
            where = {"$and": [{"name": value.upper()},{"espn_id": None}]}
            set = {"$set": {"espn_id": key}}
            coll.update_one(where, set)

print("\n\nAll done")