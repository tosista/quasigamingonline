import json
import requests
from database.getMongoClient import get_database

keys = []

with open('espnIds.json', 'r') as f:
    data = json.load(f)
    for key, value in data.items():
        keys.append(int(key))

keys.sort()

found_players = {}

'''
for n in range(5226, 6610):
    if n in keys:
        print(f"skipping {n}")
        continue
    uri = f"https://sports.core.api.espn.com/v2/sports/basketball/leagues/nba/athletes/{n}"
    response = requests.get(uri).json()
    try:
        found_players[response['id']] = response['fullName']
        print(f"added {response['fullName']}")
        with open('espnIdsMissing.txt', 'a') as f:
            f.write(f"{response['id']},{response['fullName']}\n")
    except:
        print(f"{n} is not a valid id")

'''
names_list = {}
for n in range(1, 23):
    print(f"Page {n}")
    uri = f"https://sports.core.api.espn.com/v3/sports/basketball/nba/athletes?page={n}"
    response = requests.get(uri)
    data = response.json()
    list = data['items']
    # create list of names
    for p in list:
        # print(p['id'], p['fullName'])
        names_list[p['id']] = p['fullName']
print(names_list)

# open connection to db
db = get_database()
# create the collection for players
coll = db['players']

# Create list for null_id_players
null_id_players = []

# Find players from current team and with espn_id = None
for p in coll.find({"espn_id": None}):
    null_id_players.append(p['name'])
null_id_players.sort()

i = 0
for key, value in names_list.items():
    if '.' in value:
        value = value.replace('.', '')
    if value.upper() in null_id_players:
        print(f"There he is! {value}")
        i += 1
        where = {"name": value.upper()}
        set_id = {"$set": {"espn_id": key}}
        coll.update_one(where, set_id)
print(i)
