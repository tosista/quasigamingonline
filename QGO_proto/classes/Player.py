class Player:
    def __init__(self, id, name, team, pos, value, extr, espn_id):
        self.__id = id
        self.__name = name
        self.__team = team
        # setting the positions as list
        self.__pos = pos
        self.__value = int(value)
        self.__extr = extr
        self.__espn_id = espn_id

    # Defining the properties
    @property
    def id(self):
        return self.__id

    @property
    def name(self):
        return self.__name

    @property
    def team(self):
        return self.__team

    @property
    def pos(self):
        return self.__pos

    @property
    def value(self):
        return self.__value

    @property
    def extr(self):
        return self.__extr

    @property
    def espn_id(self):
        return self.__espn_id

    # method that returns bool if given number is comprised or not
    def check_extr(self, n):
        if (n >= self.__extr[0]) and (n <= self.__extr[1]):
            return True
        else:
            return False

    # print position or positions
    def print_pos(self):
        if len(self.pos) == 1:
            return f'{self.pos[0]}'
        else:
            return f"{self.pos[0]}/{self.pos[1]}"

    # to String method
    def __str__(self):
        return f'{self.id}: {self.name}, {self.print_pos()} from {self.team}, {self.value}'

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "team": self.team,
            "pos": self.pos,
            "value": self.value,
            "extr": self.extr,
            "espn_id": self.espn_id
        }

