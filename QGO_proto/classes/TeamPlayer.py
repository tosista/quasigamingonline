from classes.Player import Player

class TeamPlayer(Player):
    def __init__(self, player, lineup_pos):
        super().__init__(player.id, player.name, player.team, player.pos, player.value, player.extr, player.espn_id)
        self.__lineup_pos = lineup_pos

    # PROPERTIES
    @property
    def lineup_pos(self):
        return self.__lineup_pos

    # METHODS

    '''
    This method sets the lineup_pos of TeamPlayer
    it takes the position as string
    First it checks if the given string is in the pos property of TeamPlayer
    It returns True or False if the operation was successful or not
    '''
    def set_lineup_pos(self, new_pos: str):
         # if pos in self.pos or if its empty string
        if new_pos in self.pos or new_pos == '':
            # set the __lineup_pos to the given string
            self.__lineup_pos = new_pos
            # return True
            return True
        # else
        else:
            # return False
            return False

    def to_dict(self):
        player_dict = super().to_dict()
        player_dict['lineup_pos'] = self.__lineup_pos
        return player_dict