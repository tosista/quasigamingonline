from classes.Lineup import Lineup
from classes.TeamPlayer import TeamPlayer

class Team:
    def __init__(self, id, nbateam, lineup=None):
        if lineup is None:
            lineup = Lineup()
        self.id = id
        self.nbateam = nbateam
        self.lineup = lineup

    # METHODS

    '''
    Method that checks if given player is in this team
    returns True if player is found, else returns False
    '''
    def check_extr_player(self, player_id):
        retvalue = False
        for p in self.lineup.lineup:
            if p.id == player_id:
                # if player is found, set return value to True
                retvalue = True

        return retvalue

    '''
    Adds a TeamPlayer to the Team object
    It takes a normal Player object
    Then it turns it into a TeamPlayer object, giving it an empty lineup_position
    '''
    # there could be a security issue here:
    # being this public, anyone could add any player to a team
    # being on the server side though, it could be ok
    def add_player(self, player):
        new_team_player = TeamPlayer(player, '')
        self.lineup.add_new_player(new_team_player)

    def __str__(self):
        string = f'id: {self.id}\nnbaTeam: {self.nbateam}\nplayers:'
        for p in self.lineup.lineup:
            string = f'{string}\n{p}'
        return string

    def to_dict(self):
        dict_players = []
        # serialize each player into a dictionary with its method
        for p in self.lineup.lineup:
            dict_players.append(p.to_dict())

        return {
            "id": self.id,
            "lineup": dict_players,
            "nbateam": self.nbateam,
        }