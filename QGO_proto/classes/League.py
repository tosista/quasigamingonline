from classes.Team import Team
from extractPlayer import extractPlayer
from database.daoNbaTeam import read_nba_team_id
import random


class League:
    def __init__(self, id, name, teams=None):
        # First partial version of the hidden properties
        if teams is None:
            teams = []

        self.__id = id
        self.__name = name
        self.__teams: list[Team] = teams

    # PROPERTIES
    @property
    def id(self):
        return self.__id

    @property
    def name(self):
        return self.__name

    @property
    def teams(self):
        return self.__teams

    # METHODS

    # Initialize the new League
    '''
    Initialize the new league
    It extracts nbateam and TeamPlayers for all 4 Team objects of the League
    '''

    def init_new_league(self):
        # array of extracted nba teams ids
        extr_nba_ids = []

        # for 4 times
        for n in range(4):
            # extract nba teams until it is not in the already extracted ids array
            extr_nbateam_id = random.randint(1, 30)
            while extr_nbateam_id in extr_nba_ids:
                extr_nbateam_id = random.randint(1, 30)

            # when we have the extracted id, fetch corresponding team
            extr_nbateam = read_nba_team_id(extr_nbateam_id)
            extr_nba_ids.append(extr_nbateam_id)

            # create a new team
            temp_team = Team(n + 1, extr_nbateam)
            self.__add_team(temp_team)
        # for each team
        for t in self.teams:
            # use the extract_n_players method to extract 15 players
            self.extract_n_players(15, t)
        for t in self.teams:
            print(t)

    '''
    Add a Team object to the __teams list
    '''
    def __add_team(self, team):
        self.__teams.append(team)

    '''
    Find a team by id and return it
    '''
    def find_team(self, id):
        found_team = None
        for t in self.__teams:
            if t.id == id:
                found_team = t
                break
        return found_team

    '''
    Extract n players and add them to the given Team
    '''
    def extract_n_players(self, num_players, team):
        # counter to keep track of successful extraction
        succ_extr = 0
        # until we successfully extracted 15 players
        while succ_extr < num_players:
            # extract a player
            # TODO add a "range" argument, to simplify player range control (for shortened matrix)
            temp_player = extractPlayer()
            # check if player is already in a team
            if self.__check_extr_player(temp_player.id):
                # if it is, extract again
                continue
            # if it is not, add it to the team players
            else:
                team.add_player(temp_player)
                succ_extr += 1

    '''
    Check extracted player
    It cycles through each Team. For each it uses the check_extr_player, providing the player_id
    That method returns True if the player is found in the Team, and False if it is not
    
    For now, this method returns True if players is already in a team, else returns False
    '''
    def __check_extr_player(self, player_id):
        retvalue = False
        for t in self.teams:
            if t.check_extr_player(player_id):
                #if the player is found, set the return value to True
                retvalue = True
        return retvalue

    # To Dictionary custom method
    def to_dict(self):
        dict_teams = []
        # transform each team into a dictionary with its custom method
        for t in self.teams:
            dict_teams.append(t.to_dict())
        return {
            "id": self.id,
            "name": self.name,
            "teams": dict_teams
        }
