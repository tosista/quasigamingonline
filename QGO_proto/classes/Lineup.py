from classes.TeamPlayer import TeamPlayer

positions = ('C', 'AG', 'AP', 'G', 'PM')

'''
This is just a function
Checks if provided position string is admissible
Meaning if it is comprised in the positions tuple
'''


def check_pos_str(pos):
    # Check if provided position exists in the positions tuple
    if pos not in positions:
        return False
    else:
        return True


class Lineup:
    def __init__(self, lineup=None):
        if lineup is None:
            lineup=[]
        self.__lineup: list[TeamPlayer] = lineup

    # PROPERTIES

    # this property returns the entire lineup
    @property
    def lineup(self):
        return self.__lineup

    # METHODS

    '''
    This method adds a new and unsorted TeamPlayer to the __lineup
    It is mainly used by the Team constructor
    '''
    # TODO is appending a new player to Lineup without checking a security issue?
    def add_new_player(self, player):
        self.__lineup.append(player)

    '''
    This method returns a filtered list with required lineup
    '''
    def read_position_lineup(self, pos):
        if check_pos_str(pos):
            return list(filter(lambda p: p.lineup_pos == pos,self.__lineup))
        else:
            return None


    '''
    This method takes the pos a string and the TeamPlayer object.
    It checks if the addition of the given player to that position is legal or not.
    It then returns True if it is legal, else it return False 
    '''
    # this could be private, maybe
    # it could also be useful to display, in frontend, only the legal positions
    def check_legal_lineup(self, test_pos: str, player: TeamPlayer):
        # filter the self.__lineup list based on player.pos == test_pos
        test_lineup = self.read_position_lineup(test_pos)
        # check if list has three players already
        if len(test_lineup) >= 3:
            return False
        else:
            # add received player to test_list
            test_lineup.append(player)
            # create the counter
            counter = 0
            # cycle through list
            for p in test_lineup:
                if p.value < 80:
                    counter += 1
                else:
                    counter += 2
            if counter <= 3:
                return True
            else:
                return False

    '''
    This methods finds the TeamPlayer from a player_id
    it return the TeamPlayer, or None
    '''
    def find_player(self, player_id: int):
        search_player = None
        # cycle through __lineup to find the player_id
        for tp in self.__lineup:
            if tp.id == player_id:
                search_player = tp
                break
        # returns the player, if not found it remains None
        return search_player

    '''
    This method takes the position as string, and the player_id as int
    First, it fetches the TeamPlayer object from the __lineup list
    Then it uses the check_legal_lineup to validate the assignment 
    If it is legal, it uses the set_lineup_pos of the TeamPlayer object, then return true
    if it is not, it returns false
    '''
    def add_lineup_player(self, pos, player_id):
        # Check if provided position exists in the positions tuple
        if not check_pos_str(pos):
            return False
        # create the edit_tp, if remains None the player_id is incorrect
        edit_player = self.find_player(player_id)
        # if the player is found, and if he has the requested position
        if edit_player and pos in edit_player.pos:
            # Invoke the check_lineup method, and if/else based on result
            if self.check_legal_lineup(pos, edit_player):
                edit_player.set_lineup_pos(pos)
                return True
            else:
                return False
        # if the player is not found in the lineup
        else:
            return False


    '''
    This method uses the set_lineup_pos to None of the TeamPlayer with given id
    '''
    # TODO check this and think about it
    def rem_lineup_player(self, player_id):
        edit_player = self.find_player(player_id)
        if edit_player:
            edit_player.set_lineup_pos('')

    '''
    Remove the unused players to clean the lineup
    '''
    def clear_unused_players(self):
        self.__lineup = list(filter(lambda p: p.lineup_pos != '', self.__lineup))
