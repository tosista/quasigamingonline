from flask import Flask, jsonify, request
from flask_cors import CORS

from extractPlayer import extractPlayer
from database.daoPlayers import readPlayers

from database.daoNbaTeam import read_nba_team

from classes.League import League
from database.daoLeague import read_league
from database.daoLeague import read_max_id
from database.daoLeague import read_all_leagues
from database.daoLeague import insertLeague
from database.daoLeague import update_league

from database import daoLineup

app = Flask(__name__)
CORS(app)

# Extract player route
@app.route('/extract', methods=['GET'])
def extract():
    serial_player = {}
    player = extractPlayer()
    if player.team != 'FA':
        serial_player = player.to_dict()
        serial_player['team'] = read_nba_team(serial_player['team'])
        print(serial_player)
    else:
        serial_player = player.to_dict()
    return jsonify(serial_player)

# Respond with the entire list of all available players
@app.route('/players', methods=['GET'])
def list_players():
    players_list = []
    # Reads the list of players from dao
    players_list_obj = readPlayers()
    # converts each Player obj to dictionary and appends it to return list
    for p in players_list_obj:
        players_list.append(p.to_dict())
    return jsonify(players_list)


# Get league with provided id
@app.route('/league', methods=['GET'])
def load_league():
    id = int(request.args.get("id"))
    league = read_league(id).to_dict()
    return jsonify(league)

# API that returns the list of all the leagues
@app.route('/listleagues', methods=['GET'])
def list_leagues():
    # create empty leagues array
    all_leagues = []
    # use daoLeagues to read all the leagues
    # the method returns a list of dictionaries
    all_leagues = read_all_leagues()

    # remove _id field to all leagues
    for l in all_leagues:
        l.pop("_id")

    return jsonify(all_leagues)

# Create new league
# TODO this endpoint
@app.route('/createleague', methods=['GET'])
def create_league():
    # Read the league's name from request
    name = request.args.get("name")
    # find out the greates league.id
    id = read_max_id() + 1
    # create new league using greatest id
    new_league = League(id, name, [])
    new_league.init_new_league()
    insertLeague(new_league)

    # read the newly added league from mongo
    added_league = read_league(id).to_dict()

    return jsonify(added_league)

# LINEUP
'''
Add given player to lineup
Receive the id of the league, the id of the team, the id and the position of the TeamPLayer to edit
Invokes the daoLineup to read the  update the mongodb
'''
@app.route('/editlineup', methods=['GET'])
def edit_lineup():
    # Create the retvalue, initialize to dictionary with KO message
    res = {'status': 'KO'}

    # Read data from the request
    id_league = int(request.args.get("idleague"))
    id_team = int(request.args.get("idteam"))
    id_player = int(request.args.get("idplayer"))
    new_pos = request.args.get("pos")


    # Read the league with correct id
    edit_league = read_league(id_league)

    # Fetch the correct team
    if edit_league:
        edit_team = edit_league.find_team(id_team)
        if edit_team:
            # check if given position is empty string (meaning user wants to remove player from lineup)
            if new_pos == '':
                # use the rem_lineup_player method from Lineup
                edit_team.lineup.rem_lineup_player(id_player)
                update_league(edit_league)
                res = {'status': 'OK'}
            else:
                if edit_team.lineup.add_lineup_player(pos=new_pos, player_id=id_player):
                    update_league(edit_league)
                    res = {'status': 'OK'}

    return jsonify(res)

@app.route('/clearunusedplayers', methods=['GET'])
def clear_unused_players():
    # Declare the response dictionary
    res = {'status': 'KO'}

    # Read id league and id team to edit
    id_league = int(request.args.get("idleague"))
    id_team = int(request.args.get("idteam"))

    # Read the league with correct id
    edit_league = read_league(id_league)

    # Fetch the correct team and clear the unused players with clear_unused_players Lineup method
    if edit_league:
        edit_team = edit_league.find_team(id_team)
        if edit_team:
            edit_team.lineup.clear_unused_players()
            update_league(edit_league)
            res = {"status": "OK"}

    return jsonify(res)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)