import random

from classes.League import League
from database import daoLeague
from database.daoNbaTeam import read_nba_team_id
from database.daoLeague import read_max_id
from database.daoLeague import read_all_leagues

if False:
    # devo far controllare prima a mongodb quale sia il prossimo id da inserire
    league = League(4, 'Silver Test')

    league.init_new_league()


    league = daoLeague.read_league(4)

    print(league)

    rand_id = random.randint(1, 30)
    nba_team = read_nba_team_id(rand_id)
    print(nba_team)

    league = League(8, 'NBATeams Test 4')

    league.init_new_league()

    print(league)

league = daoLeague.read_league(8)

max_id = read_max_id()
print(max_id)

leagues_list = read_all_leagues()