from classes.Lineup import Lineup
from classes.TeamPlayer import TeamPlayer
from database.daoPlayers import readPlayers

def add_player(pos: str, player: TeamPlayer):
    if test_lineup.add_lineup_player(pos, player.id):
        print(f'{player} added!')
    else:
        print('nope...')

def print_lineup(pos):
    print()
    print(f'The {pos} lineup is:')
    for p in test_lineup.read_position_lineup(pos):
        print(p)
    print()
    print()


giannis = readPlayers()[1]
jokic = readPlayers()[0]
bicchierone = readPlayers()[65]
favours = readPlayers()[318]
toppin = readPlayers()[159]
boucher = readPlayers()[173]

tgiannis = TeamPlayer(giannis, '')
tjokic = TeamPlayer(jokic, '')
tbicchierone = TeamPlayer(bicchierone, '')
tfavours = TeamPlayer(favours, '')
ttoppin = TeamPlayer(toppin, '')
tboucher = TeamPlayer(boucher, '')

test_lineup = Lineup([tgiannis])

test_lineup.add_new_player(tgiannis)
test_lineup.add_new_player(tjokic)
test_lineup.add_new_player(tbicchierone)
test_lineup.add_new_player(tfavours)
test_lineup.add_new_player(ttoppin)
test_lineup.add_new_player(tboucher)

add_player('C',tjokic)
add_player('C', tbicchierone)
add_player('C', tfavours)
add_player('BB',tjokic)

print_lineup('C')

# Remove test
test_lineup.rem_lineup_player(tjokic.id)

print_lineup('C')

add_player('C', tbicchierone)

print_lineup('C')

# remove all
test_lineup.rem_lineup_player(tbicchierone.id)
test_lineup.rem_lineup_player(tfavours.id)

print_lineup('C')

# adding three bronze
add_player('C', ttoppin)
add_player('C', tfavours)
add_player('C', tboucher)

print_lineup('C')

# Tentativo di aggiungere due volte
test_lineup.rem_lineup_player(tboucher.id)

print_lineup('C')

add_player('C', ttoppin)

print_lineup('C')

#  Aggiungiamo anche una AG
add_player('AG', tboucher)

print_lineup('AG')
print_lineup('C')

# Test to clear the lineup of unused
print()
print()
print('The original list of players in lineup is')
for p in test_lineup.lineup:
    print(p)

test_lineup.clear_unused_players()
print()
print()
print('The cleared list of players in lineup is')
for p in test_lineup.lineup:
    print(p)
