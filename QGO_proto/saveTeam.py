import csv

def saveTeam(teamname, players):
    with open(teamname+'.csv', 'w', newline='') as f:
        wr = csv.writer(f, delimiter=',')
        for p in players:
            wr.writerow([0,0,p.rank, p.name, p.team,p.pos,p.value,p.ext[0],p.ext[1]])
