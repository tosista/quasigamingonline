from classes.Player import Player
import sqlite3

# reads players DB and returns a Player list
def readLineup(position, id_team):
    lineup = []
    conn = sqlite3.connect('./quasigaming.db')
    cur = conn.cursor()
    sql = f"SELECT id_player FROM lineups WHERE id_team = {id_team} AND role = '{position}';"
    cur.execute(sql)
    # Create player for each table row
    for row in cur.fetchall():
        id_player = row[0]
        lineup.append(id_player)

    conn.close()

    return lineup

def add_lineup(id_team, id_player, role):

    conn = sqlite3.connect('./quasigaming.db')
    cur = conn.cursor()
    sql = f"INSERT INTO lineups (id_team, id_player, role) VALUES ({id_team}, {id_player}, '{role}');"
    cur.execute(sql)
    conn.commit()
    conn.close()

def rem_lineup(id_team, id_player):
    conn = sqlite3.connect('./quasigaming.db')
    cur = conn.cursor()
    sql = f"DELETE FROM lineups WHERE id_team = {id_team} AND id_player = {id_player};"
    cur.execute(sql)
    conn.commit()
    conn.close()

if __name__ == '__main__':
    print('not an executable')
    '''
    add_lineup(3, 132, 'AG')
    print(readLineup('AG', 3))
    rem_lineup(3, 132)
    print(readLineup('AG', 3))
    '''