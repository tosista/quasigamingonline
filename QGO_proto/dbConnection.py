import sqlite3

def get_connection():
    conn = sqlite3.connect('./quasigaming.db')
    cur = conn.cursor()
    return conn, cur