from extractNPlayers import extractNPlayers
from classes.Team import Team
import daoTeam as dt


def makeTeam():

    # cmd = int(input('How many players you want to extract? '))

    players = extractNPlayers(15)

    print('\nYour extraction is')
    for p in players:
        print(p)

    team_name = input('How do you want to call the team? ')

    # inserting data into the DB
    id_team = dt.createTeam(team_name)
    # inserting team players in DB
    for p in players:
        dt.insertTeamPlayer(id_team, p.id)

    init_lineup = {
        'C': [],
        'AG': [],
        'AP': [],
        'G': [],
        'PM': []
    }

    return Team(id_team, players, team_name, init_lineup)
