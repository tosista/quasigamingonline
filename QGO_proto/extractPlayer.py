from database.daoPlayers import readPlayers
import random

def extractPlayer():
    matrix = readPlayers()
    ext_player = {}

    # while True:
    # input('Press return to extract a player')

    # bronze end at 129666, 75 end at 34559 silver end at 6871, gold end at 169
    ext = random.randint(1,129666)

    print(ext)

    # cycle through matrix
    for p in matrix:
        # if extracted num is gte than p['min_ex'] and lte p['max_ex']
        if (p.check_extr(ext)):
            # print player
            ext_player = p
            print(f'Picked player: {p}')
            return ext_player