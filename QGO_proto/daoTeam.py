from classes.Team import Team
from classes.Player import Player
from dbConnection import get_connection
import daoLineup as dl

def createTeam(name):
    # first query to retreive max id_team in table
    conn, cur = get_connection()
    sql = "SELECT MAX(id_team) FROM teams;"
    cur.execute(sql)
    for row in cur.fetchall():
        max_id = row[0]

    # insert the new team into table
    sql = f"INSERT INTO teams (id_team, name) VALUES ({max_id+1}, '{name}');"
    cur.execute(sql)
    conn.commit()

    conn.close()

    return int(max_id) + 1

def readTeam(id_team):
    team_players = []
    team_name = ''
    team_lineup = {
        'C': [],
        'AG': [],
        'AP': [],
        'G': [],
        'PM': []
    }

    conn, cur = get_connection()

    # first we fetch the players list
    team_players = readTeamPlayers(id_team)

    # second, we fetch the team name
    team_name = findTeamName(id_team)

    # third, we fetch the lineups for each role
    # for each key value of team_lineup
    for role, list in team_lineup.items():
        # invoke daoLineup with team_id and key
        # assign return value to value
        team_lineup[role] = dl.readLineup(role, id_team)

    # Create and return the Team object
    return Team(id_team, team_players, team_name, team_lineup)


def readTeamPlayers(id_team):
    players = []

    conn, cur = get_connection()
    sql = (f"SELECT p.id_player , p.name ,p.team, p.pos , p.value , p.min_ex , p.max_ex "
           f"FROM team_players tp  INNER JOIN teams t ON tp.id_team = t.id_team "
           f"INNER JOIN players p ON p.id_player = tp.id_player "
           f"WHERE tp.id_team = {id_team} "
           f"ORDER BY p.id_player ;")

    cur.execute(sql)
    for row in cur.fetchall():
        id_player, name, team, pos, value, min_ex, max_ex = row
        tmp_player = Player(id_player, name, team, pos, value, min_ex, max_ex)
        players.append(tmp_player)

    return players

def insertTeamPlayer(id_team, id_player):
    conn, cur = get_connection()
    sql = f"INSERT INTO team_players (id_team, id_player) VALUES ({id_team}, {id_player});"
    cur.execute(sql)
    conn.commit()

def rem_team_player(id_team, id_player):
    conn, cur = get_connection()
    sql = f"DELETE FROM team_players WHERE id_team={id_team} AND id_player= {id_player};"
    cur.execute(sql)
    conn.commit()

def findTeamId(name):
    conn, cur = get_connection()
    sql = f"SELECT id_team FROM teams WHERE name = '{name}';"
    cur.execute(sql)
    for row in cur.fetchall():
        id_team = row[0]
    return id_team

def findTeamName(id):
    conn, cur = get_connection()
    sql = f"SELECT name FROM teams WHERE id_team = '{id}';"
    cur.execute(sql)
    for row in cur.fetchall():
        name = row[0]
    return name

def readTeamNames():
    teams_list = []
    conn, cur = get_connection()
    sql = f"SELECT name FROM teams ;"
    cur.execute(sql)
    for row in cur.fetchall():
        name = row[0]
        teams_list.append(name)
    return teams_list


if __name__ == '__main__':
    print('not an executable')