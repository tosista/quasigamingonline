from pymongo import MongoClient

def get_database():
    CONNECTION_STRING = "mongodb://127.0.0.1:27017/"

    client = MongoClient(CONNECTION_STRING)

    return client['quasigaming']


if __name__ == "__main__":
    # Get the database
    dbname = get_database()
    collection_name = dbname["players"]
    player1 = {
        "name": "Nikola Jokic",
        "value": 98,
        "pos": ["C"],
        "ext": [1, 1],
        "espn_id": 3112335,
        "img": "https://a.espncdn.com/combiner/i?img=/i/headshots/nba/players/full/3112335.png"
    }
    collection_name.insert_one(player1)