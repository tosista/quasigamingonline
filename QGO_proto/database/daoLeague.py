from classes.League import League
from classes.Lineup import Lineup
from classes.Team import Team
from classes.TeamPlayer import TeamPlayer
from classes.Player import Player
from database.getMongoClient import get_database
import json

# reads leagues db and return the league with provided id
def read_league(id):
    league = None
    db = get_database()
    coll = db['leagues']
    # find the league with selected id
    query = coll.find_one({"id": id})

    # Create each Team in query['teams']
    teams_obj = []
    teams_dict = query['teams']
    for t in teams_dict:
        tmp_lineup_obj = Lineup()
        for p in t['lineup']:
            tmp_player_obj = Player(p['id'], p['name'], p['team'], p['pos'], p['value'], p['extr'], p['espn_id'])
            tmp_teamplayer_obj = TeamPlayer(tmp_player_obj, p['lineup_pos'])
            tmp_lineup_obj.add_new_player(tmp_teamplayer_obj)
        tmp_team_obj = Team(t['id'], t['nbateam'], tmp_lineup_obj)
        teams_obj.append(tmp_team_obj)

    # create the league
    league = League(query['id'], query['name'], teams_obj)

    # TODO the thing here is that the return league has no actual Team object inside it
    # for now, just to return it as json is fine, but it's not enough for the backend
    return league

'''
This method still returns a list of dictionaries, and not of Leagues objects
'''
# TODO fix the return to an actual Leagues object
def read_all_leagues():
    # list of leagues to return, initialized as empty
    leagues_list = []

    db = get_database()
    coll = db['leagues']
    # query to fetch all the leagues
    query = coll.find()

    # cycle through query and add each league to the list
    for l in query:
        leagues_list.append(l)

    return leagues_list

# TODO method that reads greatest id and returns it
def read_max_id():
    db = get_database()
    coll = db['leagues']

    query = coll.find({},{ "_id": 0, "id": 1 }).sort("id", -1).limit(1)

    # TODO find a way to return zero if result is empty
    '''
    if query.retrieved == 0:
        return 0
    else:
    '''
    foud_id = 0
    for i in query:
        print(i['id'])
        # remember to cast the id to an integer, for safety
        found_id = int(i['id'])

    return found_id

def insertLeague(league: League):
    db = get_database()
    coll = db['leagues']

    # create dictionary of league
    dict_league = league.to_dict()
    print(dict_league)
    # add league to database
    query = coll.insert_one(dict_league)

def update_league(upd_league: League):
    db = get_database()
    coll = db['leagues']

    # create dictionary of league
    dict_upd_league = upd_league.to_dict()
    # print(dict_upd_league['teams'])

    # Do the update, finding the league with given id and updating the teams
    coll.update_one({'id': upd_league.id}, {'$set': {'teams': dict_upd_league['teams']}})





if __name__ == '__main__':
    print('not an executable')