from classes.Player import Player
from database.getMongoClient import get_database

# reads players DB and returns a Player list
def readPlayers():
    players = []
    db = get_database()
    coll = db['players']
    query = coll.find()
    # Create player for each table row
    for p in query:
        tmp_player = Player(
            p['id'],
            p['name'],
            p['team'],
            p['pos'],
            p['value'],
            p['extr'],
            p['espn_id']
        )
        players.append(tmp_player)

    return players

if __name__ == '__main__':
    print('not an executable')