from classes.Lineup import Lineup
from database.getMongoClient import get_database

def update_lineup(id_league, id_team, id_player, new_pos):

    # Instantiate the db
    db = get_database()
    coll = db['leagues']

    # Create filter
    filter = {{'id': id_league, 'teams':{'$elemMatch': {'id': id_team}}}}


