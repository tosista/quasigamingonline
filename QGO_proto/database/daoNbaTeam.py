from database.getMongoClient import get_database

def read_nba_team(abbr: str):
    db = get_database()
    coll = db['nbateams']

    # create query filter
    filter = {"abbreviation": abbr}

    # execute the query to mongo and fetch the team
    found_team = coll.find_one(filter)
    found_team.pop('_id')
    return found_team

# Method that takes and id and returns the correct NbaTeam (as a dict)
def read_nba_team_id(id: int):
    db = get_database()
    coll = db['nbateams']

    # create filter for team id
    filter = {"id": id}

    # execute the query and fetch the team
    found_team = coll.find_one(filter)
    # drop the mongodb _id property
    found_team.pop('_id')
    return found_team