function createPlayerCard(player) {
  // find out if player is bronze, silver or gold
  let player_color;
  if (player.value < 80) {
    player_card.classList.add('bronze');
  } else if (player.value < 90) {
    player_card.classList.add('silver');
  } else {
    player_card.classList.add('gold');
  }
  return `
      <img
        src="https://a.espncdn.com/combiner/i?img=/i/headshots/nba/players/full/${
          player.espn_id
        }.png&w=350&h=254"
        alt="${player.name}"
      />
      <div class="text_container ${player_color}">
        <h2 >${player.name}</h2>
        <h3>${player.value}</h3>
        <div class="player_team" style="background-color: #${
          player.team.color
        }; color: #${player.team.altColor}">
          <h4>${player.team.abbreviation ? player.team.abbreviation : 'FA'}</h4>
        ${player.team.logo ? `<img src=${player.team.logo} />` : ''}
        </div>
      </div>
    `;
}

function loaderOff() {
  player_loader.style.display = 'none';
}

card_container.addEventListener('load', loaderOff);

function fetchPlayer() {
  player_container.innerHTML = '';
  player_loader.style.display = 'block';
  let tmpPlayer;
  fetch('http://localhost:8000/extract')
    .then((res) => res.json())
    .then((res) => {
      tmpPlayer = res;
      let div = document.createElement('div');
      div.id = 'player_card';
      player_container.appendChild(div);
      div.innerHTML = createPlayerCard(tmpPlayer);
      setTimeout(loaderOff, 1800);
    });
}
