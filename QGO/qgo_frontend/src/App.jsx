import { useEffect, useState } from 'react';
import PlayerCard from './components/PlayerCard';
import NewLeagueRevealer from './components/NewLeagueRevealer';
import TeamPlayers from './components/TeamPlayers';
import PlayersList from './components/PlayersList';



const App = () => {
  const [player, setPlayer] = useState(null);

  const [playersList, setPlayersList] = useState(null);

  const [league, setLeague] = useState(null);

  // List Leagues states
  const [isListLeagues, setIsListLeagues] = useState(false);
  const [listLeagues, setListLeagues] = useState([]);

  // New League state
  const [newLeague, setNewLeague] = useState(null);
  const [newLeagueName, setNewLeagueName] = useState('');

  const fetchPlayer = () => {
      const uriPlayer = 'http://127.0.0.1:8000/extract';
    fetch(uriPlayer)
      .then((res) => res.json())
      .then((res) => setPlayer(res));
  };

  const fetchPlayersList = () => {
    const uriPlayersList = 'http://localhost:8000/players';

    // fetch players, then update the state of playersList
    fetch(uriPlayersList)
      .then((res) => res.json())
      .then((res) => {
        setPlayersList(res);
        // TODO change the isListLeagues to false
        setIsListLeagues(false);
      });
  };

  const fetchLeague = (id) => {
      const uriLeague = 'http://localhost:8000/league';
    console.log(`fetching league with id: ${id}`);
    fetch(uriLeague + `?id=${id}`)
      .then((res) => res.json())
      .then((res) => {
        setLeague(res);
        setIsListLeagues(false);
      });
  };

  // function that makes get request to create new league
  const createNewLeague = () => {
    console.log(`creating new league with name: ${newLeagueName}`);
    const uriCreateLeague =
      'http://localhost:8000/createleague?name=' + newLeagueName;
    console.log(uriCreateLeague);
    // fetch with get request
    fetch(uriCreateLeague)
      .then((res) => res.json())
      .then((res) => {
        setNewLeague(res);
        setIsListLeagues(false);
      });
  };

  // useEffects that fetches the list of leagues
  useEffect(() => {
    const uriListLeagues = 'http://localhost:8000/listleagues';
    console.log('fetching list of leagues');
    fetch(uriListLeagues)
      .then((res) => res.json())
      .then((res) => {
        setListLeagues(res);
        console.log('fetched leagues:');
        console.log(res);
        setIsListLeagues(true);
      });
  }, [isListLeagues]);

  if (league) {
    return (
      <>
        <h2>{league.name}</h2>
        {league.teams.map((team) => {
          return (
            <TeamPlayers team={team} idLeague={league.id} fetchLeague={fetchLeague} key={team.id} />
          );
        })}
        <button
          onClick={() => {
            setLeague(null);
            setIsListLeagues([]);
          }}
        >
          Go Back to List
        </button>
      </>
    );
  } else if (newLeague) {
    return (
      <>
        <NewLeagueRevealer teams={newLeague.teams} />
        <button
          onClick={() => {
            setLeague(null);
            setNewLeague(null);
            setIsListLeagues([]);
          }}
        >
          Go Back to List
        </button>
      </>
    );
  } else if (playersList) {
    return (
      <>
        <h2>All Players</h2>
        <button
          onClick={() => {
            setPlayersList(null);
            setIsListLeagues([]);
          }}
        >
          Go Back to Leagues List
        </button>
        <PlayersList playersList={playersList} />
      </>
    );
  } else if (isListLeagues) {
    return (
      <div className="flex_column_container">
        <h1>Quasi Gaming</h1>
        <p>by Campo Aperto</p>
        <div className="load_create_league">
          <div className="list_leagues_container">
            <h2>Load a League</h2>
            {listLeagues.map((league) => {
              return (
                <h4
                  key={league.id}
                  onClick={() => {
                    fetchLeague(league.id);
                  }}
                >
                  {league.name}
                </h4>
              );
            })}
          </div>
          <div className="create_league_container">
            <h2>Create New League</h2>
            <form
              onSubmit={(e) => {
                e.preventDefault();
                createNewLeague();
              }}
            >
              <label htmlFor="league_name"></label>
              <input
                type="text"
                name="league_name"
                id="league_name"
                value={newLeagueName}
                onChange={(e) => {
                  setNewLeagueName(e.target.value);
                }}
              />
              <button type="submit">Create league</button>
            </form>
          </div>
        </div>
        <button onClick={fetchPlayersList}>Show all players</button>
      </div>
    );
  } else {
    return (
      <>
        <h1>Loading...</h1>
      </>
    );
  }
};

export default App;
