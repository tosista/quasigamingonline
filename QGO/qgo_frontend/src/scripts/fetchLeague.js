export const fetchLeague = () => {
  let league;
  const uri = 'http://localhost:8000/league';

  fetch(uri)
    .then((res) => res.json())
    .then((res) => {
      league = res;
    });

  return league;
};
