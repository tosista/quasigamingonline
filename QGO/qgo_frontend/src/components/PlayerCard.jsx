import { useEffect, useState } from 'react';

import './PlayerCard.css';

const PlayerCard = ({
  player, idTeam, idLeague, fetchLeague
}) => {
  // Player destructuring
  const { espn_id, extr, id, lineup_pos, name, pos, team, value } = player

  // State for loading image
  const [isLoading, setIsLoading] = useState(true);
  // the image state that will contain the Image object
  const [image, setImage] = useState(null);

  // useEffect that handles img load
  // runs only on initial render
  useEffect(() => {
    const handleLoad = () => {
      setIsLoading(false);
      setImage(loadImage);
    };

    // first reset isLoading to true
    setIsLoading(true);

    // create a new Image object
    const loadImage = new Image();
    // set the Image src and the function to run on image load
    loadImage.src = `https://a.espncdn.com/combiner/i?img=/i/headshots/nba/players/full/${espn_id}.png`;
    loadImage.onload = handleLoad;

    // useEffect cleanup function
    return () => {
      loadImage.onload = null;
    };
  }, [espn_id]);

  // Function that makes the GET request to /editlineup
  const editLineup = (id_league, id_team, id_player, pos) => {
    const uriEditLineup = `http://127.0.0.1:8000/editlineup?idleague=${id_league}&idteam=${id_team}&idplayer=${id_player}&pos=${pos}`;
    console.log(uriEditLineup);
    fetch(uriEditLineup)
      .then((res) => res.json())
      .then((res) => {
        console.log(res.status);
        fetchLeague(id_league)
      });
  };

  // function that return the right color class based on player value
  const playerColor = () => {
    if (value < 80) {
      return 'bronze';
    } else if (value < 90) {
      return 'silver';
    } else {
      return 'gold';
    }
  };

  if (isLoading) {
    return <h2>Loading...</h2>;
  } else {
    return (
      <div key={id} className={'player_card ' + playerColor()}>
        <img src={image.src} alt={name} />
        <div className="text_container">
          <h3>{name}</h3>
          <div className="info_container">
            <h4>{value}</h4>
            <ul className="positions_btns_container">
              {
                lineup_pos == '' ?
                pos.map((p) => {
                  return (
                      <li key={p}>
                        <button
                            className="position_btn"
                            onClick={() => {
                              console.log(`id: ${id}, pos: ${p}`);
                              editLineup(idLeague, idTeam, id, p);
                            }}
                        >
                          {p}
                        </button>
                      </li>
                  );
                }) :
                    <button className="position_btn" onClick={() => {
                      console.log(`id: ${id}, pos: ''`);
                      editLineup(idLeague, idTeam, id, '');
                    }}>-</button>
              }

            </ul>
            <div className="player_team">
              <p>{team ? team : `FA`}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default PlayerCard;
