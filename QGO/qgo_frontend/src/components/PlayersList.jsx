import PlayerCard from './PlayerCard';
import './PlayersList.css';

const PlayersList = ({ playersList }) => {
  return (
    <div className="playerslist_grid">
      {playersList.map((player) => {
        return <PlayerCard player={player} key={player.id} />;
      })}
    </div>
  );
};

export default PlayersList;
