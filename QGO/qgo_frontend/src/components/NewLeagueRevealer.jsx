import { useState } from 'react';
import PlayerCard from './PlayerCard';
import TeamPlayers from './TeamPlayers';

const NewLeagueRevealer = ({ teams }) => {
  console.log('inside NewTeamRevealer we have teams:');
  console.log(teams);

  // State values
  // current team to reveal, represents index of teams array
  const [currTeamIndex, setCurrTeamIndex] = useState(0);

  // boolean switch to reveal NBA Team
  const [isNbaTeamRev, setIsNbaTeamRev] = useState(false);

  // current index to trim players [], increases as new player is revealed
  const [revPlayerIndex, setRevPlayerIndex] = useState(0);

  if (isNbaTeamRev && revPlayerIndex < teams[currTeamIndex].lineup.length) {
    // to the TeamPlayers component we give a slice of the lineup
    // to hide also the first player, we give the slice(0, 0) using a ternary operator
    // before giving the slice (by overriding the parameter) we use the spread operator to pass all properties of current Team
    return (
      <>
        <h2>Team {currTeamIndex + 1}</h2>
        <TeamPlayers
          team={{
            ...teams[currTeamIndex],
            lineup: teams[currTeamIndex].lineup.slice(
              revPlayerIndex == 0 ? 0 : revPlayerIndex - 1,
              revPlayerIndex
            ),
          }}
        />
        <button onClick={() => setRevPlayerIndex(revPlayerIndex + 1)}>
          Extract next player
        </button>
      </>
    );
  } else if (
    isNbaTeamRev &&
    revPlayerIndex >= teams[currTeamIndex].lineup.length
  ) {
    return (
      <>
        <h2>Team {currTeamIndex + 1}</h2>
        <TeamPlayers
          team={{
            ...teams[currTeamIndex],
            lineup: teams[currTeamIndex].lineup.slice(0, revPlayerIndex),
          }}
        />
        <button
          onClick={() => {
            setCurrTeamIndex(currTeamIndex + 1);
            setIsNbaTeamRev(false);
            setRevPlayerIndex(0);
          }}
        >
          Go to next team
        </button>
      </>
    );
  } else if (currTeamIndex >= teams.length) {
    return <></>;
  } else {
    return (
      <>
        <h2>Team {currTeamIndex + 1}</h2>
        <button onClick={() => setIsNbaTeamRev(true)}>reveal nba team</button>
      </>
    );
  }
  // need to go to next page when currTeamIndex == teams.length
};

export default NewLeagueRevealer;
