import { useState } from 'react';

import './TeamPlayers.css';
import Lineup from './Lineup';

// this function renders the given lineup[] if it is not empty
// it also takes the position as string, to render the subtitle of the lineup
const renderLineupForPos = (lineup, pos, idTeam, idLeague, fetchLeague) => {
  const filteredLineup = lineup.filter((p) => p.lineup_pos == pos);
  if (filteredLineup.length > 0) {
    return (
      <div>
        <h4>{pos == '' ? 'Unused' : pos}</h4>
        <Lineup lineup={filteredLineup} idTeam={idTeam} idLeague={idLeague} fetchLeague={fetchLeague} />
      </div>
    );
  }
};

// Function that validates the linep, and removes the unsorted players
const clearUnusedPlayers = (id_league, id_team, fetchLeague) => {
  const uriClearUnused = `http://127.0.0.1:8000/clearunusedplayers?idleague=${id_league}&idteam=${id_team}`;
  console.log(uriClearUnused);

  fetch(uriClearUnused)
    .then((res) => res.json())
    .then((res) => {
      console.log(res)
      fetchLeague(id_league)
    });
};

const TeamPlayers = ({team, idLeague, fetchLeague}) => {
  let { id, lineup, nbateam } = team;
  // current index to trim players [], increases as new player is revealed
  const [revPlayerIndex, setRevPlayerIndex] = useState(0);

  // sort the lineup
  lineup = lineup.sort((a, b) => {
    return b.value - a.value;
  });

  return (
    <div
      style={{ backgroundColor: `#${nbateam.color}` }}
      className="team_container"
    >
      <img src={nbateam.logo} alt={nbateam.displayName} className="nba_logo" />
      <h2 className="nbateam_name">{nbateam.displayName}</h2>
      {renderLineupForPos(lineup, 'C', id, idLeague, fetchLeague)}
      {renderLineupForPos(lineup, 'AG', id, idLeague, fetchLeague)}
      {renderLineupForPos(lineup, 'AP', id, idLeague, fetchLeague)}
      {renderLineupForPos(lineup, 'G', id, idLeague, fetchLeague)}
      {renderLineupForPos(lineup, 'PM', id, idLeague, fetchLeague)}
      {renderLineupForPos(lineup, '', id, idLeague, fetchLeague)}
      <button
        onClick={() => {
          clearUnusedPlayers(idLeague, id, fetchLeague);
        }}
      >
        Clear unused players
      </button>
    </div>
  );
};

export default TeamPlayers;
