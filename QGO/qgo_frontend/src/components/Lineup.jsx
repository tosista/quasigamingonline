import PlayerCard from './PlayerCard';
import './TeamPlayers.css';

const Lineup = ({ lineup, idTeam, idLeague, fetchLeague }) => {
  return (
    <div className="players_grid">
      {lineup.map((player) => {
        return <PlayerCard player={player} idTeam={idTeam} idLeague={idLeague} fetchLeague={fetchLeague} key={player.id} />;
      })}
    </div>
  );
};

export default Lineup;
